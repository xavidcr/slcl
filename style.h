#ifndef STYLE_H
#define STYLE_H

#include <stddef.h>

extern const char style_default[];
extern const size_t style_default_len;

#endif
