mark_as_advanced(WEB_LIBRARY WEB_INCLUDE_DIR)
find_library(WEB_LIBRARY NAMES libweb web)

find_path(WEB_INCLUDE_DIR
    NAMES
        handler.h
        html.h
        http.h
        server.h
        wildcard_cmp.h
    PATH_SUFFIXES libweb include/libweb)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(web
    DEFAULT_MSG WEB_LIBRARY WEB_INCLUDE_DIR)

if(WEB_FOUND)
    if(NOT TARGET web)
        add_library(web UNKNOWN IMPORTED)
        set_target_properties(web PROPERTIES
            INTERFACE_INCLUDE_DIRECTORIES "${WEB_INCLUDE_DIR}"
            IMPORTED_LOCATION "${WEB_LIBRARY}")
    endif()
endif()
